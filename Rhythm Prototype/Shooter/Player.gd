extends KinematicBody2D

signal player_death

const MAX_SPEED : float = 16500.0
const ACCEL : float = 12500.0
const MAX_LIFE : int = 10
const IFRAMES : int = 20
const BASE_SCALE : Vector2 = Vector2(1,1)
var velocity : Vector2 = Vector2(0,0)
var life : int
var bonus : int
var multiplier : int
var invincible : bool = false
var can_dodge : bool = true
var is_dodging : float = 1.0
var iframes : int = 0

export var tutorial : bool = false
export var tutorial_dodge : bool = false
export var direction : Vector2 = Vector2(0,0)

const SLIGHT_CRACKED = preload("res://Assets/Sprites/shield1.png")
const CRACKED = preload("res://Assets/Sprites/shield2.png")
const VERY_CRACKED = preload("res://Assets/Sprites/shield3.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	life = MAX_LIFE
	bonus = 0
	multiplier = 1

func get_direction() -> Vector2:
	$Ship/ShipBody/LeftParticles.emitting = false
	$Ship/ShipBody/RightParticles.emitting = false
	if tutorial:
		return direction
	var dir = Vector2(0,0)
	if Input.is_action_pressed("ui_down"):
		dir.y += 1
	if Input.is_action_pressed("ui_up"):
		dir.y -= 1
	if Input.is_action_pressed("ui_left"):
		dir.x -= 1
	if Input.is_action_pressed("ui_right"):
		dir.x += 1
	if dir != Vector2(0,0):
		$Ship/ShipBody/LeftParticles.emitting = true
		$Ship/ShipBody/RightParticles.emitting = true
	return dir.normalized()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	iframes += 1
	if invincible:
		$Ship/ShipBody/Body.color.a = 1 - ($InvicibilityTimer.time_left - int($InvicibilityTimer.time_left))
	velocity += get_direction() * ACCEL * delta * is_dodging
	if (Input.is_action_just_pressed("rhythm_action_1") and can_dodge) or tutorial_dodge:
		can_dodge = false
		$DodgeTimer.start()
		$Animations.play("dodge")
		if not tutorial:
			AUDIO.play_se("DODGE", -20)
		is_dodging = 2.75
		iframes = 0
	velocity = 0.89*velocity.clamped(MAX_SPEED)
	$Ship.rotation = PI/2 + velocity.angle()
	$Ship/ShipBody.scale = BASE_SCALE * GLOBAL.beat_scale
	$Collision.rotation = PI/2 + velocity.angle()
	velocity = move_and_slide(velocity)

func check_game_over():
	if life == 0:
		AUDIO.play_se("EXPLOSION", -8)
		print("GAME OVER")
		queue_free()
		emit_signal("player_death")

func update_shield():
	AUDIO.play_se("CRACK", -8)
	if life == 1:
		# add shield breaking animation
		$Hitbox/Shield.hide()
	elif life <= 2:
		$Hitbox/Shield.texture = VERY_CRACKED
	elif life <= 5:
		$Hitbox/Shield.texture = CRACKED
	elif life <= 8:
		$Hitbox/Shield.texture = SLIGHT_CRACKED

func _on_Hitbox_body_entered(body):
	if body.is_in_group("notes"):
		body.queue_free()
		multiplier = 1
		if not invincible and iframes > IFRAMES:
			life = life - 1
			print("PLAYER_HIT ", life)
			update_shield()
			invincible = true
			$InvicibilityTimer.start()
			check_game_over()


func _on_InvicibilityTimer_timeout():
	invincible = false
	$Ship/ShipBody/Body.color.a = 1.0


func _on_DodgeTimer_timeout():
	can_dodge = true
	is_dodging = 1.0

var expected_life = MAX_LIFE

func _on_Bonus_body_entered(body):
	if body.is_in_group("notes"):
		expected_life = life


func _on_Bonus_body_exited(body):
	if body.is_in_group("notes") and expected_life == life and not invincible:
		$CloseCall.set_text("Close call!\n+"+str(multiplier*500))
		$Animations.play("close_call")
		bonus += multiplier
		multiplier += 1
		$BonusTimer.start()


func _on_BonusTimer_timeout():
	multiplier = 1

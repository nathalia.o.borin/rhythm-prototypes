extends Node

# Global variables containing all loaded songs
var SONG = null
var BEATMAP = null
var GAME_MODE = 0
var DIFICULTY = 0

const MODE_NAMES = ["Connect", "Shooter"]

var initial_map : Array = []
var beat_scale : Vector2 = Vector2(1,1)
var color_scale : Color = Color(1, 1, 1, 1)

# Helper function to get the Root node
func get_root():
	return get_node("/root/Root")

# Helper Function: returns a list of files on a directory
# pointed by the path argument
func list_files_in_directory(path: String, ext: String) -> PoolStringArray:
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with(".") and file.ends_with(ext):
			files.append(file)
	
	dir.list_dir_end()
	return files

func load_metadata_info(path: String) -> Dictionary:
	var file = File.new()
	print("Trying to read "+path)
	file.open(path, file.READ)
	var text = file.get_as_text()
	var result_json = JSON.parse(text)
	if result_json.error == OK:  # If parse OK
		var data = result_json.result
		return data
	return {}

func save_song_metadata(path: String, metadata: Dictionary) -> void:
	var file = File.new()
	print("Trying to write "+path)
	file.open(path, file.WRITE)
	file.store_line(to_json(metadata))
	file.close()

func update_metadata(song: Song) -> void:
	var metadata = {
		'title': song.title,
		'artist': song.artist,
		'bpm': song.BPM,
		'length': song.duration,
		'onsets': song.onsets,
		'high_score': song.high_score
	}
	save_song_metadata(song.metadataPath, metadata)

func parse_list(path: String) -> Array:
	var file = File.new()
	file.open(path, File.READ)
	var content = file.get_as_text()
	file.close()
	var list = content.split(',')
	var ret = []
	for n in list:
		ret.append(float(n.dedent()))
	return ret

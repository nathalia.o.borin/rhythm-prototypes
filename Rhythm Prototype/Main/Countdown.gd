extends Control

signal countdown_ended

func init(seconds: float) -> void:
	$Timer.wait_time = seconds
	$Timer.start()
	$Label.show()

func _physics_process(delta):
	$Label.set_text(str(int($Timer.time_left)+1))

func _on_Timer_timeout():
	$Label.hide()
	emit_signal("countdown_ended")
	set_process(false)

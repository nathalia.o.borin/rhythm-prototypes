from godot import exposed, export, Array, Dictionary, Node

import os
import sys
import librosa
from mutagen.easyid3 import EasyID3
from pydub import AudioSegment
from .FMP import compute_energy, compute_complex, compute_spectral, join_onsets, intersect_onsets

@exposed
class BeatMapper(Node):

	# member variables here, example:
	#onsets = export(Function)

	def _ready(self):
		"""
		Called every time the node is added to the scene.
		Initialization here.
		"""
		print("PRINTING SYSTEM PATH")
		print(sys.path)
		print(os.getcwd())
		AudioSegment.converter = os.getcwd()+"/lib/ffmpeg"
		print("CONVERTER PATH")
		print(AudioSegment.converter)
	def read_song_metadata(self, path):
		audio = EasyID3(str(path))
		sound = AudioSegment.from_mp3(str(path))
		oggFile = str(path).replace('.mp3', '.ogg')
		sound.export(oggFile, format='ogg')
		y, Fs = librosa.load(str(oggFile))
		tempo, beats = librosa.beat.beat_track(y)
		duration = librosa.get_duration(y, Fs)
		beats_sec = librosa.frames_to_time(beats)
		title = 'Unknown'
		artist = 'Unknown'
		if 'title' in audio:
			title = audio['title'][0]
		if 'artist' in audio:
			artist = ", ".join(audio['artist'])
		return Array([title,
			artist,
			tempo,
			duration,
			Array(beats_sec.tolist())
		])
	def calculate_onsets(self, path, game_mode, difficulty):
		y, Fs = librosa.load(str(path))
		if game_mode == 0:
			if difficulty == 0:
				e = compute_energy(y, Fs)
				s = compute_spectral(y, Fs)
				beatmap = intersect_onsets(e, s, 0.1)
			elif difficulty == 2:
				e = compute_energy(y, Fs)
				c = compute_complex(y, Fs)
				beatmap = join_onsets(e, c, 0.1)
			elif difficulty == 3:
				e = compute_energy(y, Fs)
				s = compute_spectral(y, Fs)
				beatmap = join_onsets(e, s, 0.1)
		elif game_mode == 1:
			if difficulty == 1:
				beatmap = compute_spectral(y, Fs).tolist()
			elif difficulty == 2:
				c = compute_complex(y, Fs)
				s = compute_spectral(y, Fs)
				beatmap = join_onsets(s, c, 0.01)
			elif difficulty == 3:
				e = compute_energy(y, Fs)
				s = compute_spectral(y, Fs)
				beatmap = join_onsets(e, s, 0.1)
		return Array(beatmap)

extends Node

class_name Projectile

const sources = [
	Vector2(-785, -580),
	Vector2(-585, -580),
	Vector2(-385, -580),
	Vector2(-185, -580),
	Vector2(15, -580),
	Vector2(215, -580),
	Vector2(415, -580),
	Vector2(615, -580),
	Vector2(815, -580)
]

const randomPos = 0.1
const keepMomentum = 0.65

# All time units in ms
var spawnTime : float
var position: Vector2
var speed : float = 800

var direction: int
var position_index : int

const DOWN_VECTOR = Vector2(0, 1)

# Called when the node enters the scene tree for the first time.
func _init(spawnTimeArg: float, prevPos: int, prevDir: int):
	self.spawnTime = spawnTimeArg
	randomize()
	var random = randf()
	var chosenDir = prevPos
	if random > randomPos:
		if random <= keepMomentum:
			if prevDir > 0:
				chosenDir = prevPos + 1
			else:
				chosenDir = prevPos - 1
		else:
			chosenDir = prevPos + int(rand_range(-1, 1))
		if chosenDir == -1:
			chosenDir = 0
		elif chosenDir == len(sources):
			chosenDir = len(sources) - 1
	else:
		chosenDir = randi() % len(sources)
	var chosenPosition = sources[chosenDir]
	self.position = chosenPosition
	self.direction = prevPos - chosenDir
	self.position_index = chosenDir

extends Node
class_name PlayerInput

var buttonAction: String

# Called when the node enters the scene tree for the first time.
func _init(action: String = "rhythm_action_1"):
	if randf() <= 0.5:
		action = 'rhythm_action_2'
	self.buttonAction = action

static func get_all_actions() -> Array:
	var allActions = ['rhythm_action_1', 'rhythm_action_2']
	return allActions

static func get_color(action: String) -> Color:
	var allColors = {'rhythm_action_1': Color.red, 'rhythm_action_2': Color.blue}
	return allColors[action]
